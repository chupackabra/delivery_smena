# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from django.utils.encoding import  python_2_unicode_compatible

from django.db import models
from  django.db.models import Sum

# Create your models here.
@python_2_unicode_compatible
class Dish (models.Model):
    TYPES= (
        ('soup', "Суп"),
        ('second', "Второе"),
        ('salad', "Салат"),
        ('dessert', "Десерт"),
    )
    name = models.CharField(max_length=255)
    weight = models.PositiveIntegerField()
    calorie = models.PositiveIntegerField()
    price = models.PositiveIntegerField()
    type = models.CharField(max_length=255, choices=TYPES, default='soup')

    class Meta():
        verbose_name = 'Блюдо'
        verbose_name_plural = 'Блюда'

    def __str__(self):
        return self.name

@python_2_unicode_compatible
class Dinner (models.Model):
    name = models.CharField(max_length=255)
    dishes = models.ManyToManyField(Dish)
    image = models.ImageField()

    class Meta():
        verbose_name = "Обед"
        verbose_name_plural = "Обеды"

    def __str__(self):
        return self.name

    @property
    def cost (self):
        cost_as_dict=self.dishes.all().aggregate(Sum('price'))
        return cost_as_dict['price__sum']

    @property
    def calories(self):
        calories_as_dict = self.dishes.all().aggregate(Sum('calorie'))
        return calories_as_dict['calorie__sum']

    @property
    def total_weight(self):
        weight_as_dict = self.dishes.all().aggregate(Sum('weight'))
        return weight_as_dict['weight__sum']

    @property
    def dinner_soup(self):
        return self.dishes.get(type='soup')

    @property
    def dinner_salad(self):
        return self.dishes.get(type='salad')

    @property
    def dinner_second(self):
        return self.dishes.get(type='second')

    @property
    def dinner_dessert(self):
        return self.dishes.get(type='dessert')

@python_2_unicode_compatible
class DailyMenu(models.Model):
    date = models.DateField()
    dinners = models.ManyToManyField(Dinner)

    class Meta:
        verbose_name = 'Меню на день'
        verbose_name_plural = 'Меню на день'

    def __str__(self):
        return 'Меню на день {}'. format(self.date)