# -*- coding: utf-8 -*-
# Generated by Django 1.11.2 on 2017-06-06 12:01
from __future__ import unicode_literals

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('dishes', '0001_initial'),
    ]

    operations = [
        migrations.RenameModel(
            old_name='Daily_Menu',
            new_name='DailyMenu',
        ),
        migrations.AlterModelOptions(
            name='dailymenu',
            options={'verbose_name': '\u041c\u0435\u043d\u044e \u043d\u0430 \u0434\u0435\u043d\u044c', 'verbose_name_plural': '\u041c\u0435\u043d\u044e \u043d\u0430 \u0434\u0435\u043d\u044c'},
        ),
    ]
