basketData = {}
routeData = {}

$(function () {
    // Форма ввода времени доставки скрыта
    $('.form__address').on('submit', function (event) {
        event.preventDefault();
        basketData.address = $(event.target).find('input.input__this').val();
        $('.order__address').hide();
        $('.order__time').show();

    });

    // Код для добавления обедов в корзину
    $('.item-quantity-form button').on('click', function (event) {
        var action;
        if ($(this).hasClass('plus')) {
            action = 'increase';
        }
        else {
            action = 'decrease';
        }
        var data = {
            item_id: $(this).parents('form').find('[name="item_id"]').val(),
            csrfmiddlewaretoken: $(this).parents('form').find('[name="csrfmiddlewaretoken"]').val(),
            action: action
        };
        // Количество позиций и цена отображаются без перезагруски страницы
        $.ajax({
            url: $(this).parents('form').attr('action'),
            data: data,
            method: "POST"

        }) .done(
           function (response) {
               if (response) {
                   $(this).parents('.lunch__control-cont').find('.lunch__control-count').text(response.quantity);
                   $('.cart__count').text(response.position);
                   $('.cart__cost').text(response.price);
               }
            }.bind(this)

        );
        event.preventDefault();
    });



    // Данные адреса, полученные из suggestion(сервис dadata), передаются на бекэнд
    $('.form__address').on('submit', function (event) {
        event.preventDefault();
        var data = {
            longitude: basketData.longitude,
            latitude: basketData.latitude,
            csrfmiddlewaretoken: $(this).find('[name="csrfmiddlewaretoken"]').val()
        };


        $.ajax({
                url: $('.form__address').attr('action'),
                method: "POST",
                data: data,

        });

    });
    // Прокрутка до формы логина и регистрации
    $('.button-login').on('click', function (event) {

        $(this).scroll('.main__form')

    });
        //Карта на странице деталей заказа
        ymaps.ready(function () {
            var myMap = new ymaps.Map("YMapsID", {
                // Центр карты
                center: [54.7430600, 55.9677900],
                         // Коэффициент масштабирования
                zoom: 13,
                         // Тип карты
                type: "yandex#map"
            });

            myMap.controls.add("mapTools")
                    // Добавление кнопки изменения масштаба
            .add("zoomControl")
                    // Добавление списка типов карты
            .add("typeSelector");
            basketData.map = myMap;
        });

        //Код для отображения курьера по маршруту доставки
        $(document).ready(function () {

            if ($('#route_id').text()) {
                var intervalId = setInterval(function show() {
                        var data = {'order_id': $('#order_id').text()};

                        $.ajax({
                            url: "/get_route/",
                            method: 'GET',
                            data: data,
                        }).done(function (response) {
                            var myMap = basketData.map;
                            if (routeData.lon != response.lon && routeData.lat != response.lat) {
                                var balloon = myMap.balloon.open([response.lat, response.lon], {content: "Курьер уже везет ваш заказ!"}, { closeButton: false });
                                routeData = {'lon': response.lon, 'lat': response.lat}

                            }
                            else {
                                var balloon = myMap.balloon.open([response.lat, response.lon], {content: "Тук-тук!"});
                                clearInterval(intervalId)
                            }


                        });

                    }
                    , 3000)
            }
        });




 })