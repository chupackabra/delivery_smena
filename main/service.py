#-*- coding: utf-8 -*-

import re
import random
import requests

from basket.models import Order
from main.models import Code


class CheckPhone(object):

    @classmethod
    def re_phone(cls, phone):
        phone_as_list = re.findall(r'[0-9]{2,}', phone)
        phone = ''.join(phone_as_list)
        return phone

class GetRouteId(object):

    @classmethod
    def get_route_id(cls, order_id):
        order = Order.objects.get(id=order_id)
        data = {'lon': order.address.longitude, 'lat': order.address.latitude }
        res = requests.post('http://cme.frfrdev.ru/route/create/', json=data)
        try:
            current_res=res.json()
            try:
                current_res['status'] == 'OK'
                order.courier_id = current_res['id']
                order.save()
                return order
            except KeyError:
                return order
        except ValueError:
            return order





class GetCurrentPoint(object):

    @classmethod
    def get_cur_point(cls, order_id):
        order = Order.objects.get(id=order_id)
        res = requests.get('http://cme.frfrdev.ru/route/{}/current_point'.format(order.courier_id))
        current_response = res.json()
        if current_response.get('status') == 'OK':
            current_response = current_response['point']
            return current_response


class VerificationCodeService(object):

    @classmethod
    def regen_key(cls, phone):
        new_code = random.randint(100000, 999999)
        Code.objects.create(code=new_code, phone=phone)
        return new_code


class SmsCode(object):
    host = 'https://sms.ru/sms/send/'

    @classmethod
    def send_sms (cls, phone, code):
        data = {
            'api_id': 'f567a71e-f7b2-c134-0da6-4c6bbd49602b',
            'to': phone,
            'msg': u"Ваш код для регистрации на сервисе доставки обедов Kotletas {}".format(code),
            'json': 1
        }
        res = requests.post(cls.host, params=data)
        res = res.json()
        res = res['sms'][phone]['status_code']
        return res
