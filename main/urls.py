from django.conf.urls import url

from . import views



urlpatterns =[
    url(r'weekly_menu/', views.WeeklyMenuListView.as_view(), name='weekly_menu'),
    url(r'^$', views.HomeListView.as_view(), name='home'),
    url(r'registration/', views.RegistrationFormView.as_view(), name='registration'),
    url(r'login/', views.LoginFormView.as_view(), name='login'),
    url(r'logout/', views.LogoutView.as_view(), name='logout'),
    url(r'change_password/', views.ChangePasswordFormView.as_view(), name='change_password'),
    url(r'change_items/', views.BasketChangeQuantityView.as_view(), name='basket_change'),
    url(r'address/', views.AddressTemplateView.as_view(), name='address'),
    url(r'time/', views.TimeDeliveryView.as_view(), name='time'),
    url(r'order/(?P<order_id>\d+)/$', views.OrderDetailView.as_view(), name='order_detail'),
    url(r'profile/', views.ProfileView.as_view(), name='profile'),
    url(r'get_route/', views.GetCurrentCourierView.as_view(), name = 'get_point'),

]