# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models
from django.contrib.auth.models import User
# Create your models here.


class Client (models.Model):
    phone = models.CharField(max_length=20, unique=True)
    user = models.OneToOneField(User)


class Code (models.Model):

    code = models.CharField(max_length=6)
    phone = models.CharField(max_length=20)
    created_at = models.DateTimeField(auto_now=True)
    active = models.BooleanField(default=False)



