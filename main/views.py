# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from datetime import datetime, timedelta


from django.contrib.auth.decorators import login_required
from django.contrib.auth.models import User
from django.shortcuts import  redirect
from django.http import HttpResponseRedirect, JsonResponse, HttpResponse
from django.contrib.auth import login, authenticate, logout
from django.contrib.auth.hashers import check_password
from django.utils.decorators import method_decorator
from django.views import View
from django.views.decorators.http import require_POST
from django.views.generic import ListView, FormView, DetailView, TemplateView
from django.core.urlresolvers import reverse
from django import forms

from dishes.models import DailyMenu
from main.forms import RegistrationForm, LoginForm, ChangePasswordForm
from .models import Client, Code
from basket.models import Basket, Order, OrderItem, Address
from .service import GetCurrentPoint, GetRouteId, VerificationCodeService, SmsCode


class WeeklyMenuListView(ListView):
    model = DailyMenu
    today = datetime.today()
    template_name = 'main/weekly_menu.html'

    def get_context_data(self, **kwargs):
        context = super(WeeklyMenuListView, self).get_context_data(**kwargs)
        if self.today.weekday() <= 6:
            context['start'] = self.today - timedelta(days=self.today.weekday())
            context['finish'] = context['start'] + timedelta(days=6)
        else:
            context['start'] = self.today-timedelta(days=self.today.weekday())+timedelta(days=7)
            context['finish'] = context['start']+timedelta(days=6)
        context['menues'] = DailyMenu.objects.filter(date__gte=context['start'], date__lte=context['finish']).order_by('date')
        return context


class HomeListView(TemplateView):

    template_name = 'main/home.html'

    def get_queryset(self):
        daily_menu = DailyMenu.objects.get(date=datetime.today())
        return daily_menu



class RegistrationFormView(FormView):
    template_name = 'main/home.html'
    form_class = RegistrationForm
    success_url = '/'

    def get_form_kwargs(self):
        kwargs = super(RegistrationFormView, self).get_form_kwargs()
        kwargs.update({
            'request': self.request
        })
        return kwargs

    def form_valid(self, form):

        phone = form.cleaned_data.get('phone')
        #Если нажата кнопта "отправить смс", для веденного номера генерируется рандомный код, который отправляется на sms.ru
        if self.request.POST.get('button') == 'send_sms':
            new_code = VerificationCodeService.regen_key(phone)
            #вызывается сервис для проверки ответа сервиса и вывода возможной ошибки
            res = SmsCode.send_sms(code=new_code, phone=phone)
            if res != 100:
                form.add_error('phone', forms.ValidationError(u'Вы ввели нерабочий номер телефона'))
                return super(RegistrationFormView, self).form_invalid(form)

        if self.request.POST.get('button') == 'check_in':
            try:
                Client.objects.get(phone=phone)
            except Client.DoesNotExist:
                new_user = User.objects.create_user(username=phone, password=form.cleaned_data.get('password'))
                Client.objects.create(phone=phone, user=new_user)
                login(self.request, new_user)
        return super(RegistrationFormView, self).form_valid(form)




class LoginFormView(FormView):
    template_name = 'main/home.html'
    form_class = LoginForm
    success_url = '/profile/'



    def form_valid(self, form):
        phone=form.cleaned_data.get('phone')
        try:
            Client.objects.get(phone=phone)
            user = authenticate(self.request, username=phone, password=form.cleaned_data.get('password'))
            if user:
                login(self.request, user)
                return super(LoginFormView, self).form_valid(form)
            else:
                form.add_error(None, forms.ValidationError(u'Неверный пароль'))
                return super(LoginFormView, self).form_invalid(form)
        except Client.DoesNotExist:
            return redirect ('home')


class LogoutView(View):

    def post(self, request):
        logout(request)
        return redirect('home')


class ChangePasswordFormView(FormView):
    template_name = 'main/profile.html'
    form_class = ChangePasswordForm
    success_url = '/'

    @method_decorator(login_required)
    def dispatch(self, request, *args, **kwargs):
        return super(ChangePasswordFormView, self).dispatch(request, *args, **kwargs)

    def form_valid(self, form):
        if check_password(form.cleaned_data.get('current_password'), self.request.user.password):
            self.request.user.set_password(form.cleaned_data.get('new_password'))
            self.request.user.save()
        return super(ChangePasswordFormView, self).form_valid(form)




class BasketChangeQuantityView(View):

    @method_decorator(require_POST)
    def dispatch(self, request, *args, **kwargs):
        return super(BasketChangeQuantityView, self).dispatch(request, *args, **kwargs)

    def post(self, request):
        item_id = request.POST.get('item_id')
        basket = Basket.objects.get(id=request.basket.id)
        item = basket.items.get(id=item_id)
        if request.POST.get('action') == 'decrease':
            item.quantity = item.quantity - 1 if item.quantity > 0 else 0
        else:
            item.quantity = item.quantity + 1
        item.save()

        js_dict = {'quantity': item.quantity,'position': basket.positions, 'price': basket.sum_cost }
        if request.is_ajax():
            return JsonResponse (js_dict)

        else:
            return redirect('home')


class AddressTemplateView(View):


    @method_decorator(require_POST)
    def dispatch(self, request, *args, **kwargs):
        return super(AddressTemplateView, self).dispatch(request, *args, **kwargs)

    def post(self, request, *args, **kwargs):
        basket = Basket.objects.get(id=request.basket.id)
        cur_address = Address.objects.create(
                address=request.POST.get('address'),
                longitude=request.POST.get('longitude'),
                latitude=request.POST.get('latitude'),
            )
        basket.address = cur_address
        basket.save()
        return HttpResponse(status=201)


class TimeDeliveryView(View):

    @method_decorator(require_POST)
    def dispatch(self, request, *args, **kwargs):
        return super(TimeDeliveryView, self).dispatch(request, *args, **kwargs)

    def post(self, request, *args, **kwargs):
        try:
            minute = request.POST.get('minute')
            hour = request.POST.get('hour')
            basket = Basket.objects.get(id=request.basket.id)
            if hour and minute:
                basket.delivery_at = datetime.today().replace(hour=int(hour), minute=int(minute))
                basket.save()
                order = Order.objects.create(client=self.request.user.client, address=basket.address, delivery_at=basket.delivery_at)
                #Вызывается метод, который делает запрос на "http://cme.frfrdev.ru", для получения id маршрута курьера
                GetRouteId.get_route_id(order_id=order.id)
                return HttpResponseRedirect (reverse('order_detail', kwargs= {'order_id': order.id}))
            else:
                return  redirect('home')
        except ValueError:
            return redirect('home')



class OrderDetailView(DetailView):
    model = Order
    template_name = 'main/detail.html'
    pk_url_kwargs = 'order_id'
    query_pk_and_slug = True


    def get_object(self, queryset=None):
        return Order.objects.get(id=self.kwargs['order_id'])


    def get_context_data(self, **kwargs):
        order = Order.objects.get(id=self.kwargs['order_id'])
        context = super(OrderDetailView, self).get_context_data(**kwargs)
        basket_items = self.request.basket.items.exclude(quantity=0)

        if not basket_items:
            return context
        for item in basket_items:
            try:
                OrderItem.objects.get(order=order, dinner=item.dinner, quantity=item.quantity)
            except OrderItem.DoesNotExist:
                    OrderItem.objects.create(order=order, dinner=item.dinner, quantity=item.quantity)
        context['time_now'] = order.created_at+timedelta(hours=5)
        context['items'] = order.orderitem_set.all()



        return context

class GetCurrentCourierView(View):

    def get(self, request):
        order_id = request.GET.get('order_id')
        current_response = GetCurrentPoint.get_cur_point(order_id=order_id)
        return JsonResponse(current_response)


class ProfileView(ListView):

    template_name = 'main/profile.html'
    paginate_by = 10
    context_object_name = 'orders'

    def get_queryset(self):
        orders = Order.objects.filter(client=self.request.user.client).exclude(orderitem__quantity__isnull=True).order_by('-delivery_at')
        return orders








