# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from basket.models import Basket

def basket_h(request):
    basket_id =request.session.get('basket_id')
    try:
        basket_h = Basket.objects.get(id=basket_id)
    except Basket.DoesNotExist:
        basket_h = 0
    return {'basket_h': basket_h}



def basket(request):
    return {'basket' : request.basket}