# -*- coding: utf-8 -*-
from __future__ import unicode_literals



from django.contrib import admin

from dishes.models import Dish, Dinner, DailyMenu
from models import Client, Code
from basket.models import Basket, BasketItems, Order, OrderItem, Address

# Register your models here.

@admin.register(Dish)
class DishAdmin(admin.ModelAdmin):
    list_display=['name', 'weight', 'calorie', 'type' , 'price']

@admin.register(Dinner)
class DinnerAdmin(admin.ModelAdmin):
    filter_horizontal =['dishes']

@admin.register(DailyMenu)
class DailyMenuAdmin(admin.ModelAdmin):
    list_display=['date']
    filter_horizontal=['dinners']

@admin.register(Client)
class ClientAdmin(admin.ModelAdmin):
    pass


@admin.register(Basket)
class BasketAdmin(admin.ModelAdmin):
    pass


@admin.register(BasketItems)
class BasketItemsAdmin(admin.ModelAdmin):
    pass


@admin.register(Order)
class OrderAdmin(admin.ModelAdmin):
    pass


@admin.register(OrderItem)
class OrderItemsAdmin(admin.ModelAdmin):
    pass


@admin.register(Address)
class AddressAdmin(admin.ModelAdmin):
    pass


@admin.register(Code)
class CodeAdmin(admin.ModelAdmin):
    pass

