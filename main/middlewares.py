import datetime

from basket.models import Basket, BasketItems
from dishes.models import DailyMenu


class BasketMiddleware(object):

    def __init__(self, get_response):
        self.get_response = get_response

    def __call__(self, request):
        basket = self._get_basket(request)
        setattr(request, 'basket', basket)
        response = self.get_response(request)

        return response

    def _get_basket(self, request):
        basket_id = request.session.get('basket_id')
        dinners = DailyMenu.objects.get(date=datetime.datetime.today()).dinners.all()
        try:
            basket = Basket.objects.get(id=basket_id)
            if request.user.is_authenticated:
                basket.client = request.user.client
                basket.save()
        except Basket.DoesNotExist:
            if request.user.is_authenticated:
                basket = Basket.objects.create(client=request.user.client)
            else:
                basket = Basket.objects.create()
            for dinner in dinners:
                BasketItems.objects.create(basket=basket, dinner=dinner, quantity=0)
            request.session['basket_id'] = basket.id
        return basket


