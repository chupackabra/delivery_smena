# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django import forms

from main.service import CheckPhone
from main.models import Code


class RegistrationForm(forms.Form):
    phone = forms.CharField(max_length=16, required=False)
    password = forms.CharField(max_length=255, required=False)
    code = forms.CharField(max_length=6, required=False)

    def __init__(self, request, *args, **kwargs):
        self.request = request
        super(RegistrationForm, self).__init__(*args, **kwargs)

    def clean_phone(self):
        phone = self.data['phone']
        phone = CheckPhone.re_phone(phone)
        if len(phone)<9:
            raise forms.ValidationError(u'Номер телефона должен содержать 10 символов')
        return phone


    def _exists_code(self):
        try:
            Code.objects.get(phone=self.cleaned_data.get('phone'), code=self.cleaned_data.get('code'))
        except Code.DoesNotExist:
            raise forms.ValidationError(u'Неверный код')


    def clean(self):
        if self.request.POST.get('button') =='check_in':
            self._exists_code()
        else:
            self.clean_phone()
        super(RegistrationForm, self).clean()
        return self.cleaned_data



class LoginForm(forms.Form):
    phone = forms.CharField(max_length=16)
    password = forms.CharField(max_length=255)

    def clean_phone(self):
        phone = self.cleaned_data['phone']
        phone = CheckPhone.re_phone(phone)
        return phone



class ChangePasswordForm(forms.Form):
    current_password = forms.CharField(max_length=255)
    new_password = forms.CharField(max_length=255)
    new_password_again = forms.CharField(max_length=255)

    def clean(self):
        new_password = self.data.get('new_password')
        new_password_again = self.data.get('new_password_again')
        if new_password != new_password_again:
            raise forms.ValidationError(u"Пароли должны совпадать. Попробуйте еще раз.")
        return self.cleaned_data





