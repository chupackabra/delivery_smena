# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from datetime import timedelta


from django.core.management.base import BaseCommand

from dishes.models import DailyMenu


class Command (BaseCommand):

    help = 'Add Daily Menu to next week'

    def handle(self, *args, **options):
        current_weekly_menu = DailyMenu.objects.all().order_by('-date')[:7]
        for day_menu in current_weekly_menu:
            d = DailyMenu.objects.create(date=day_menu.date + timedelta(days=7))
            day_dinners = day_menu.dinners.all()
            for dinner in day_dinners:
                d.dinners.add(dinner)







