# -*- coding: utf-8 -*-
# Generated by Django 1.11.2 on 2017-08-03 14:16
from __future__ import unicode_literals

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('basket', '0009_order_courier_id'),
    ]

    operations = [
        migrations.RenameField(
            model_name='address',
            old_name='city',
            new_name='address',
        ),
        migrations.RemoveField(
            model_name='address',
            name='flat',
        ),
        migrations.RemoveField(
            model_name='address',
            name='house',
        ),
        migrations.RemoveField(
            model_name='address',
            name='street',
        ),
    ]
