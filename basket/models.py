# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models
from django.db.models import Sum


from main.models import Client
from dishes.models import Dinner

# Create your models here.

class Address(models.Model):
    address = models.CharField(max_length=255, null=True)
    longitude = models.FloatField(max_length=255, null=True)
    latitude = models.FloatField(max_length=255, null=True)



class Basket(models.Model):
    client = models.ForeignKey(Client, null=True)
    address = models.ForeignKey(Address, null =True, blank=True)
    created_at = models.DateTimeField(auto_now=True)
    delivery_at = models.DateTimeField(null=True)

    class Meta:
        verbose_name = 'Корзина'
        verbose_name_plural = 'Корзины'

    @property
    def price(self):
        items = self.items.all()
        cost = 0
        try:
            for item in items:
                cost += item.dinner.cost * int(item.quantity)
            return cost
        except Basket.DoesNotExist:
            return cost

    @property
    def positions(self):
        pos_as_dict = self.items.all().aggregate(Sum('quantity'))

        return pos_as_dict['quantity__sum']

    @property
    def sum_cost(self):
        items = self.items.all()
        cost=0
        for item in items:
            cost+=item.dinner.cost*int(item.quantity)
        return cost



class BasketItems (models.Model):
    dinner = models.ForeignKey(Dinner)
    basket = models.ForeignKey(Basket, related_name='items')
    quantity = models.IntegerField(default=0)


class Order (models.Model):
    client = models.ForeignKey(Client)
    created_at = models.DateTimeField(auto_now=True)
    delivery_at = models.DateTimeField()
    status = models.BooleanField(default=False)
    address = models.ForeignKey(Address)
    courier_id = models.IntegerField(null=True)

    @property
    def sum_cost(self):
        items = self.orderitem_set.all()
        cost=0
        for item in items:
            cost+=item.dinner.cost*int(item.quantity)
        return cost



class OrderItem(models.Model):
    order = models.ForeignKey(Order)
    dinner = models.ForeignKey(Dinner)
    quantity = models.IntegerField()



